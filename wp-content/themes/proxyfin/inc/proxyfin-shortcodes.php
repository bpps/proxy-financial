<?php

function get_philosophy_page( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
    <div class="financial-page-container">
      <div class="section-header">
        <h1><?php echo $atts['title']; ?></h1>
        <?php print $atts['header']; ?>
      </div>
      <div class="proxy-logos-holder">
        <section id="proxy-section-capital" class="proxy-section">
          <div class="proxy-section-content">
            <a href="<?php echo get_the_permalink(182); ?>">
              <h1 class="proxy-section-label">
                <?php echo $atts['capital-slogan']; ?>
              </h1>
              <div class="proxy-section-logo">
                <?php display_proxy_logo_toggle('capital'); ?>
              </div>
              <!-- <div class="proxy-section-links">
                <a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '' : home_url(); echo '#private-equity'; ?>">
                  Private Equity
                </a>
                <a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '' : home_url(); echo '#eb-5'; ?>">
                  EB-5
                </a>
              </div> -->
            </a>
          </div>
        </section>
        <section id="proxy-section-wealth" class="proxy-section">
          <div class="proxy-section-content">
            <a href="<?php echo get_the_permalink(180); ?>">
              <h1 class="proxy-section-label">
                <?php echo $atts['wealth-slogan']; ?>
              </h1>
              <div class="proxy-section-logo">
                <?php display_proxy_logo_toggle('wealth'); ?>
              </div>
              <!-- <div class="proxy-section-links">
                <a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '' : home_url(); echo '#asset-management'; ?>">
                  Asset Management
                </a>
                <a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '' : home_url(); echo '#financial-planning'; ?>">
                  Financial Planning
                </a>
                <a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '' : home_url(); echo '#fund-distribution'; ?>">
                  Fund Distribution
                </a>
              </div> -->
            </a>
          </div>
        </section>
      </div>
    </div>
  <?php
  return ob_get_clean();
}
add_shortcode( 'philosophy', 'get_philosophy_page' );

function get_team_page( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
  <div id="team-wrapper">
    <h1><?php echo $atts['title']; ?></h1>
    <?php
    $teamtypes = get_terms(['taxonomy' => 'team-type', 'hide_empty' => true]);
    foreach($teamtypes as $teamtype) {
      $args = array(
        'post_type' => 'team',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'tax_query' => [
          [
            'taxonomy' => 'team-type',
            'field' => 'slug',
            'terms' => $teamtype->slug
          ]
        ]
      );
      $team_query = new WP_Query($args);
      if($team_query->have_posts()) : ?>
        <div id="team-<?php echo $teamtype->slug; ?>-container" class="team-container">
          <?php
          while($team_query->have_posts()): $team_query->the_post();
            $teamcount = $team_query->post_count;
            $memberImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium'); ?>
            <div class="team-member-container">
              <div class="member-image-container">
                <div class="member-image bg-centered" style="background-image:url(<?php echo $memberImage; ?>);">
                </div>
                <div class="member-info">
                  <h3 class="member-name"><?php the_title(); ?></h3>
                  <?php if($memberTitle = get_field('position')) { ?>
                    <span class="member-title"><?php echo $memberTitle; ?></span>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php
          endwhile; ?>
        </div>
      <?php
      endif;
    } ?>
  </div>
  <?php
  return ob_get_clean();
}

add_shortcode( 'team', 'get_team_page' );

function show_landing_page($atts) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
  <div class="full-width-page">
    <?php
    $id = get_option('page_on_front');
    $hero = get_the_post_thumbnail_url($id, 'large'); ?>
    <div class="page-header-wrapper">
      <div class="page-header-text">
        <div class="content-inner">
          <?php echo get_the_excerpt($id); ?>
        </div>
      </div>

      <div class="hero-image bg-centered" style="background-image:url('<?php echo $hero; ?>');">
      </div>
    </div>
    <div class="page-content">
      <div class="content-inner">
        <div class="home-icons">
          <?php
          $icons = get_field('homepage_icons', 'option');
          foreach($icons as $icon) { ?>
            <div class="home-icon-wrapper">
              <?php
              if($link = $icon['link']) { ?>
                <a href="<?php echo $link; ?>">
              <?php
              } ?>
                <div class="home-icon" style="background-image:url(<?php echo $icon['icon']['sizes']['medium']; ?>);">
                </div>
                <h3><?php echo $icon['title']; ?></h3>
              <?php
              if($icon['link']) { ?>
                </a>
              <?php
              } ?>
            </div>
          <?php
          } ?>
        </div>
      </div>
    </div>
  </div>
  <?php
  return ob_get_clean();
}

add_shortcode( 'home-landing', 'show_landing_page');

function show_proxy_landing_page($atts) {
  $a = shortcode_atts( array(), $atts );
  global $post;
  ob_start();
  $id = $post->ID; ?>
  <div class="page-content">
    <div class="content-inner">
      <div class="two-columns-container">
        <div class="column-1 page-section-column">
          <div class="page-column-content">
            <?php echo get_field('content_2', $id); ?>
          </div>
        </div>
        <div class="column-2 page-section-column">
          <div class="proxy-landing-logo">
            <div class="page-column-content">
              <?php display_proxy_logo_toggle($atts['proxy']); ?>
              <h3 class="proxy-tag-line" style="text-align:center; font-style: italic;">
                <?php echo get_the_excerpt($id); ?>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  return ob_get_clean();
}

add_shortcode( 'proxy-landing', 'show_proxy_landing_page');
?>
