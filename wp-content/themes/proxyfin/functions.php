<?php
/**
 * proxyfin functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package proxyfin
 */

if ( ! function_exists( 'proxyfin_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function proxyfin_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on proxyfin, use a find and replace
		 * to change 'proxyfin' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'proxyfin', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_post_type_support( 'page', 'excerpt' );

		add_image_size( 'small', 400 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'proxyfin' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'proxyfin_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'proxyfin_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function proxyfin_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'proxyfin_content_width', 640 );
}
add_action( 'after_setup_theme', 'proxyfin_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function proxyfin_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'proxyfin' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'proxyfin' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'proxyfin_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function proxyfin_scripts() {

	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'proxyfin-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('proxyfin-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

	wp_enqueue_style( 'fullpage-style', get_template_directory_uri() .'/js/fullpage/jquery.fullPage.css',false, null);

	wp_enqueue_script( 'fullpage-script', get_template_directory_uri() .'/js/fullpage/jquery.fullPage.js', ['jquery'], null, true);

	wp_enqueue_script( 'fullpage-overflow', get_template_directory_uri() .'/js/fullpage/vendors/scrolloverflow.min.js', ['jquery'], null, true);

	wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );

	wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );
	//wp_enqueue_script( 'proxyfin-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	//wp_enqueue_script( 'proxyfin-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'proxyfin_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
	* NYCJ Shortcodes.
	*/
require get_template_directory() . '/inc/proxyfin-shortcodes.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'Proxy Settings',
		'menu_slug' 	=> 'proxy-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Translations',
		'menu_title'	=> 'Translations',
		'parent_slug'	=> 'proxy-settings',
	));

}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function register_custom_post_types() {

	$labels = array(
    'name' => _x('Team', 'post type general name'),
    'singular_name' => _x('Team Member', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New Team Member'),
    'edit_item' => __('Edit Team Member'),
    'new_item' => __('New Team Member'),
    'view_item' => __('View Team Member'),
    'search_items' => __('Search Team'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
    'taxonomies' => array('team-type'),
  );

  register_post_type( 'team' , $args );
}

add_action('init', 'register_custom_post_types');

function register_custom_taxonomies() {

	$labels = array(
		'name'                       => _x( 'Team Types', 'taxonomy general name', 'proxyfin' ),
		'singular_name'              => _x( 'Team Type', 'taxonomy singular name', 'proxyfin' ),
		'search_items'               => __( 'Search Team Types', 'proxyfin' ),
		'popular_items'              => __( 'Popular Team Types', 'proxyfin' ),
		'all_items'                  => __( 'All Team Types', 'proxyfin' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Team Type', 'proxyfin' ),
		'update_item'                => __( 'Update Team Type', 'proxyfin' ),
		'add_new_item'               => __( 'Add New Team Type', 'proxyfin' ),
		'new_item_name'              => __( 'New Team Type', 'proxyfin' ),
		'separate_items_with_commas' => __( 'Separate team types with commas', 'proxyfin' ),
		'add_or_remove_items'        => __( 'Add or remove team types', 'proxyfin' ),
		'choose_from_most_used'      => __( 'Choose from the most used team types', 'proxyfin' ),
		'not_found'                  => __( 'No team types found.', 'proxyfin' ),
		'menu_name'                  => __( 'Team Types', 'proxyfin' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'team-type' ),
	);

	register_taxonomy( 'team-type', array('team'), $args );
	register_taxonomy_for_object_type( 'team-type', array('team') );
}

add_action('init', 'register_custom_taxonomies', 0);
