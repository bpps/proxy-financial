jQuery.noConflict();
(function( $ ) {
  $(function() {
    var $slickInstance;
    var eventImgInt;
    var totalImages = 0;
    var currentImgInd = 3;
    var eventImgCount;
    var eventImgCounter = 0;

    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
        isMobile = true;
    }
    $(document).ready(function() {
      $('.intro-logo').addClass('active');
      setTimeout(function() {
        $('.intro-overlay').fadeOut(1000);
      }, 3000);
      $('body').addClass('loaded');
      if($('body').hasClass('hide-intro') && !$('body').hasClass('home')) {
        $('.hashtag-link, .site-branding').toggleClass('active');
      }
      //console.log(menuItems);
      // $('#team-primary-container .team-member-container').on('mouseenter', function(){
      //   $('.team-member-container').removeClass('active inactive');
      //   $(this).addClass('active');
      //   $('#team-primary-container .team-member-container').not(this).addClass('inactive');
      // });
      if($('#fullpage').length) {
        $('#fullpage').fullpage({
          anchors: menuItems.slugs,
          menu: '#anchor-menu',
          loopHorizontal: false,
          scrollOverflow: true,
          sectionSelector: '.section',
          //paddingTop: $('.site-header').height(),
          //navigation: true,
	        //navigationPosition: 'right',
          //navigationTooltips: menuItems.menu,
          normalScrollElements: '#lhnHelpOutCenter,#live-chat-wrapper',
	        showActiveTooltip: false,
          onLeave: function(index, nextIndex, direction){
            if($('body').hasClass('menu-active')){
              $('body').toggleClass('menu-active');
            }
            var $nextSection = $('.fp-section').eq(nextIndex -1);
            var sectionSlug = $('.fp-section').eq(nextIndex).data('anchor');
            var color = $($nextSection).data('color');
            var pageid = $($nextSection).data('pageid');
            if(color != 'transparent') {
              color = 'rgb('+color+')';
            }
            console.log(nextIndex, color);
            $('#wp-admin-bar-edit a').attr('href', `${homeurl}/wp-admin/post.php?post=${pageid}&action=edit`);
            $('body').css('background-color', color);
        		checkLogo(nextIndex);

            // if($('.fp-section.active .fp-scrollable').length) {
            //   var IScroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
            //   console.log(IScroll);
            //   IScroll.scrollTo(0, 0, 0);
            // }
            // console.log($('.fp-section').length);
            // if(nextIndex === $('.fp-section').length) {
            //   //$.fn.fullpage.moveTo(index-1);
            //   triggerContact();
            //   moveSectionUp();
            //
            // }

        	},
          afterLoad: function(origin, destination, direction) {
            $('#fullpage').removeClass('hidden');
            $('.loader').fadeOut('fast');
            $('li[data-menuanchor="contact-us"], a[href="#contact-us"]').on('click', function(e) {
              e.preventDefault();
              triggerContact();
            });
            checkLogo(destination);

          }
        });

        if($('.connect-flower').length) {
          $('.connect-flower').on('mouseenter click', function(e) {
            e.preventDefault();
            triggerContact();
          });
        }

        function triggerContact() {
          if(!$('#contact-info').hasClass('active')) {
            $('#contact-info').addClass('active');
            setTimeout(function(){
              if(!$('#contact-info').is(":hover")) {
                $('#contact-info').removeClass('active');
              }
            }, 5000);
          }
          return false;
        }

        $('.section').each(function(){
          var $thisSection = this;
          if($('.fp-next', $thisSection).length){
            var $nextSlide = $('.slide:first-child',$thisSection).next('.slide');
            var nextTitle = $('.section-title', $nextSlide).html();
            $('.fp-next', $thisSection).html(nextTitle);
          }
        });
        if($('.next-page-button').length) {
          $('.next-page-button').on('click', function() {
            $('#fullPage').fullpage.moveSectionDown();
          });
        }
      }

      function checkLogo(index) {
        var $headerLogo = $('.header-logo-container .header-text');
        if(index === 1) {
          if(!$($headerLogo).is(':visible')) {
            $($headerLogo).slideDown();
          }
        } else {
          if($($headerLogo).is(':visible')) {
            $($headerLogo).slideUp();
          }
        }
      }

      if($('.sub-sub-title').length) {
        $('.sub-sub-title').on('click', function(e) {
          e.preventDefault();
          var $subContent = $(this).next('.sub-sub-content-wrapper');
          if($($subContent).hasClass('active')) {
            $($subContent).removeClass('active');
          } else {
            $('.sub-sub-content-wrapper.active').removeClass('active');
            $($subContent).addClass('active');
          }
          window.dispatchEvent(new Event('resize'));
        });
      }

      if($('.sub-sub-menu-item').length) {
        $('.sub-sub-menu-item').on('click', function(e) {
          e.preventDefault();
          var toggleSlug = $(this).data('slug');
          if(!$('.sub-sub-section.'+toggleSlug).hasClass('active')) {
            $('.sub-sub-menu-item.active').removeClass('active');
            $(this).addClass('active');
            $('.sub-sub-section.active').removeClass('active');
            $('.sub-sub-section.'+toggleSlug).addClass('active');
          }
          window.dispatchEvent(new Event('resize'));
        });
      }

      if(!isMobile) {
        $('.menu-toggle').on('mouseenter', function() {
          if(!$('body').hasClass('menu-active')) {
            $('body').addClass('menu-active');
          }
        });
      }
      $('.menu-overlay').on('mouseleave', function() {
        if($('body').hasClass('menu-active')) {
          $('body').removeClass('menu-active');
        }
      });
      $('.menu-toggle').on('click', function() {
        $('body').toggleClass('menu-active');
      });
      $('.connect-flower-toggle').on('click', function(){
        return false;
      });
      $('#contact-info').on('mouseleave', function(){
        if($('#contact-info').hasClass('active')) {
          $('#contact-info').removeClass('active');
        }
      });
      $('#meeting-button a').on('click', function(e) {
        e.preventDefault();
        $('#book-meeting-wrapper').show();
      });
      if($('#close-book-meeting').length) {
        $('#close-book-meeting').on('click', function(e){
          e.preventDefault();
          $('#book-meeting-wrapper').hide();
        });
      }

    });

    if($('.join-list').length){
      $('.join-list').on('click', function(e) {
        e.preventDefault();
        $('body').addClass('signup-active');
      });
    }

    if($('.jw-submit').length){
      $('.jw-submit').on('click', function(e) {
        e.preventDefault();
        $('.jw-email').removeClass('error');
        var email = $('.jw-email').val();
        if(email.length){
          $('#newsletter-container').addClass('loading');
          signup_user(email);
        } else {
          $('.jw-email').addClass('error');
        }
      });
    }

    $('#close-live-chat').on('click', function(e){
      $('body').removeClass('live-chat-active');
    });

    $('#liveagent-button').on('click', function(e){
      e.preventDefault();
      $('body:not(.live-chat-active)').addClass('live-chat-active');
    })

    window.lhnJsSdkInit = function () {
  		lhnJsSdk.setup = {
  			application_id: "c111e851-e2cb-47fc-bede-c18c26c49042",
  			application_secret: "dtx+wc6dl/tbg5xk3tnrxnaehhia/c0n6qndkzr+kebp2tkozs"
  		};
      lhnJsSdk.openCallback = function() {

      };
  		lhnJsSdk.controls = [{
  			type: "hoc",
  			id: "09321de4-15bb-49d4-9410-475e2eea00df"
  		}];
  	};

  	(function (d, s) {
  		var newjs, lhnjs = d.getElementsByTagName(s)[0];
  		newjs = d.createElement(s);
  		newjs.src = "https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js";
  		lhnjs.parentNode.insertBefore(newjs, lhnjs);
  	}(document, "script"));

    function signup_user(email) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'sign_up_user',
          'email' : email
        }
      }).done( function (response) {
        $('#newsletter-container').removeClass('loading');
        response = $.parseJSON(response);
        $('#newsletter-signup-text').html('<div class="response-text">'+response.response+'</div>');
        $('.jw-email').val('');
      });
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    // if (!window._laq) {
    //   window._laq = [];
    // }
    // window._laq.push(function(){
    //   liveagent.showWhenOnline('573f2000000G1J5', document.getElementById('liveagent_button_online_573f2000000G1J5'));
    //   liveagent.showWhenOffline('573f2000000G1J5', document.getElementById('liveagent_button_offline_573f2000000G1J5'));
    // });

  });
})(jQuery);
