<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package proxyfin
 */
 $pageParent = wp_get_post_parent_id( get_the_ID() );
 if ($pageParent != 0) {
 	wp_redirect(get_permalink($pageParent).'#'.basename(get_the_permalink(get_the_ID())));
 } else {
 // Do whatever templating you want as a fall-back.
 }
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$args = [
				'post_parent' => get_the_ID(),
				'post_type'   => 'page',
				'numberposts' => -1,
				'post_status' => 'publish'
			];
			$children = get_children( $args );
			if($children) {
				$type = get_field('layout'); ?>
				<div id="fullpage" class="hidden">
					<div class="loader">
						<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
					</div>
					<section data-pageid="<?php the_ID(); ?>" class="section first-section section-<?php echo $type; ?> fp-auto-height" id="section-<?php basename(get_the_permalink()); ?>">
						<div class="section-wrapper">
							<div class="full-width-page">
								<?php the_content(); ?>
							</div>
						</div>
					</section>
					<?php echo get_page_sections(get_the_ID()); ?>
				</div>
			<?php
		} else {
			// This is where regular page stuff goes.
		}

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
