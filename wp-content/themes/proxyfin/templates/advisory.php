<?php
/**
 * Template Name:  Advisory Page
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package proxyfin
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
      $args = array(
        'post_type' => 'advisor',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
      );
      $advisors = new WP_Query($args);
      if($advisors->have_posts()) : ?>
        <section class="section">
          <div class="section-wrapper">
            <div id="team-container">
              <?php
              while($advisors->have_posts()):
                $advisors->the_post();
								if(get_the_content()) {
	                $advisorImage = get_the_post_thumbnail_url(get_the_ID(), 'small-medium');
	                $slug = slugify(get_the_title()); ?>
	                <div class="team-member-container" name="<?php echo $slug; ?>">
	                  <a class="team-member-anchor" name="<?php echo $slug; ?>"></a>
	                  <div class="member-image-container">
	                    <div class="member-image bg-centered" style="background-image:url(<?php echo $advisorImage; ?>);">
	                    </div>
	                  </div>
	                  <div class="member-content">
	                    <h3 class="member-name"><?php the_title(); ?></h3>
	                    <?php if($memberTypes = get_the_terms(get_the_ID(), 'advisor-type')) { ?>
	                      <span class="member-title"><?php echo $memberTypes[0]->name; ?></span>
	                    <?php } ?>
	                    <?php the_content(); ?>
	                  </div>
	                </div>
	              <?php
								}
              endwhile; ?>
            </div>
          </div>
        </section>
      <?php
      endif;
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
