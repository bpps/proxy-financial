/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);
__webpack_require__(2);
//require('slick-carousel')
__webpack_require__(3);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_0__factory, __WEBPACK_LOCAL_MODULE_0__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * imagesLoaded PACKAGED v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

!function (e, t) {
   true ? !(__WEBPACK_LOCAL_MODULE_0__factory = (t), (__WEBPACK_LOCAL_MODULE_0__module = { id: "ev-emitter/ev-emitter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_0__ = (typeof __WEBPACK_LOCAL_MODULE_0__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_0__factory.call(__WEBPACK_LOCAL_MODULE_0__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_0__module.exports, __WEBPACK_LOCAL_MODULE_0__module)) : __WEBPACK_LOCAL_MODULE_0__factory), (__WEBPACK_LOCAL_MODULE_0__module.loaded = true), __WEBPACK_LOCAL_MODULE_0__ === undefined && (__WEBPACK_LOCAL_MODULE_0__ = __WEBPACK_LOCAL_MODULE_0__module.exports)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t() : e.EvEmitter = t();
}("undefined" != typeof window ? window : undefined, function () {
  function e() {}var t = e.prototype;return t.on = function (e, t) {
    if (e && t) {
      var i = this._events = this._events || {},
          n = i[e] = i[e] || [];return n.indexOf(t) == -1 && n.push(t), this;
    }
  }, t.once = function (e, t) {
    if (e && t) {
      this.on(e, t);var i = this._onceEvents = this._onceEvents || {},
          n = i[e] = i[e] || {};return n[t] = !0, this;
    }
  }, t.off = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      var n = i.indexOf(t);return n != -1 && i.splice(n, 1), this;
    }
  }, t.emitEvent = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      i = i.slice(0), t = t || [];for (var n = this._onceEvents && this._onceEvents[e], o = 0; o < i.length; o++) {
        var r = i[o],
            s = n && n[r];s && (this.off(e, r), delete n[r]), r.apply(this, t);
      }return this;
    }
  }, t.allOff = function () {
    delete this._events, delete this._onceEvents;
  }, e;
}), function (e, t) {
  "use strict";
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i) {
    return t(e, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter);
}("undefined" != typeof window ? window : undefined, function (e, t) {
  function i(e, t) {
    for (var i in t) {
      e[i] = t[i];
    }return e;
  }function n(e) {
    if (Array.isArray(e)) return e;var t = "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "number" == typeof e.length;return t ? d.call(e) : [e];
  }function o(e, t, r) {
    if (!(this instanceof o)) return new o(e, t, r);var s = e;return "string" == typeof e && (s = document.querySelectorAll(e)), s ? (this.elements = n(s), this.options = i({}, this.options), "function" == typeof t ? r = t : i(this.options, t), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred()), void setTimeout(this.check.bind(this))) : void a.error("Bad element for imagesLoaded " + (s || e));
  }function r(e) {
    this.img = e;
  }function s(e, t) {
    this.url = e, this.element = t, this.img = new Image();
  }var h = e.jQuery,
      a = e.console,
      d = Array.prototype.slice;o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this);
  }, o.prototype.addElementImages = function (e) {
    "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e);var t = e.nodeType;if (t && u[t]) {
      for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var o = i[n];this.addImage(o);
      }if ("string" == typeof this.options.background) {
        var r = e.querySelectorAll(this.options.background);for (n = 0; n < r.length; n++) {
          var s = r[n];this.addElementBackgroundImages(s);
        }
      }
    }
  };var u = { 1: !0, 9: !0, 11: !0 };return o.prototype.addElementBackgroundImages = function (e) {
    var t = getComputedStyle(e);if (t) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) {
      var o = n && n[2];o && this.addBackground(o, e), n = i.exec(t.backgroundImage);
    }
  }, o.prototype.addImage = function (e) {
    var t = new r(e);this.images.push(t);
  }, o.prototype.addBackground = function (e, t) {
    var i = new s(e, t);this.images.push(i);
  }, o.prototype.check = function () {
    function e(e, i, n) {
      setTimeout(function () {
        t.progress(e, i, n);
      });
    }var t = this;return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) {
      t.once("progress", e), t.check();
    }) : void this.complete();
  }, o.prototype.progress = function (e, t, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, e, t);
  }, o.prototype.complete = function () {
    var e = this.hasAnyBroken ? "fail" : "done";if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var t = this.hasAnyBroken ? "reject" : "resolve";this.jqDeferred[t](this);
    }
  }, r.prototype = Object.create(t.prototype), r.prototype.check = function () {
    var e = this.getIsImageComplete();return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image(), this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void (this.proxyImage.src = this.img.src));
  }, r.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth;
  }, r.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.img, t]);
  }, r.prototype.handleEvent = function (e) {
    var t = "on" + e.type;this[t] && this[t](e);
  }, r.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents();
  }, r.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents();
  }, r.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;var e = this.getIsImageComplete();e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents());
  }, s.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.element, t]);
  }, o.makeJQueryPlugin = function (t) {
    t = t || e.jQuery, t && (h = t, h.fn.imagesLoaded = function (e, t) {
      var i = new o(this, e, t);return i.jqDeferred.promise(h(this));
    });
  }, o.makeJQueryPlugin(), o;
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery.noConflict();
(function ($) {
  $(function () {
    var $slickInstance;
    var eventImgInt;
    var totalImages = 0;
    var currentImgInd = 3;
    var eventImgCount;
    var eventImgCounter = 0;

    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
      isMobile = true;
    }
    $(document).ready(function () {
      $('.intro-logo').addClass('active');
      setTimeout(function () {
        $('.intro-overlay').fadeOut(1000);
      }, 3000);
      $('body').addClass('loaded');
      if ($('body').hasClass('hide-intro') && !$('body').hasClass('home')) {
        $('.hashtag-link, .site-branding').toggleClass('active');
      }
      //console.log(menuItems);
      // $('#team-primary-container .team-member-container').on('mouseenter', function(){
      //   $('.team-member-container').removeClass('active inactive');
      //   $(this).addClass('active');
      //   $('#team-primary-container .team-member-container').not(this).addClass('inactive');
      // });
      if ($('#fullpage').length) {
        var triggerContact = function triggerContact() {
          if (!$('#contact-info').hasClass('active')) {
            $('#contact-info').addClass('active');
            setTimeout(function () {
              if (!$('#contact-info').is(":hover")) {
                $('#contact-info').removeClass('active');
              }
            }, 5000);
          }
          return false;
        };

        $('#fullpage').fullpage({
          anchors: menuItems.slugs,
          menu: '#anchor-menu',
          loopHorizontal: false,
          scrollOverflow: true,
          sectionSelector: '.section',
          //paddingTop: $('.site-header').height(),
          //navigation: true,
          //navigationPosition: 'right',
          //navigationTooltips: menuItems.menu,
          normalScrollElements: '#lhnHelpOutCenter,#live-chat-wrapper',
          showActiveTooltip: false,
          onLeave: function onLeave(index, nextIndex, direction) {
            if ($('body').hasClass('menu-active')) {
              $('body').toggleClass('menu-active');
            }
            var $nextSection = $('.fp-section').eq(nextIndex - 1);
            var sectionSlug = $('.fp-section').eq(nextIndex).data('anchor');
            var color = $($nextSection).data('color');
            var pageid = $($nextSection).data('pageid');
            if (color != 'transparent') {
              color = 'rgb(' + color + ')';
            }
            console.log(nextIndex, color);
            $('#wp-admin-bar-edit a').attr('href', homeurl + '/wp-admin/post.php?post=' + pageid + '&action=edit');
            $('body').css('background-color', color);
            checkLogo(nextIndex);

            // if($('.fp-section.active .fp-scrollable').length) {
            //   var IScroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
            //   console.log(IScroll);
            //   IScroll.scrollTo(0, 0, 0);
            // }
            // console.log($('.fp-section').length);
            // if(nextIndex === $('.fp-section').length) {
            //   //$.fn.fullpage.moveTo(index-1);
            //   triggerContact();
            //   moveSectionUp();
            //
            // }
          },
          afterLoad: function afterLoad(origin, destination, direction) {
            $('#fullpage').removeClass('hidden');
            $('.loader').fadeOut('fast');
            $('li[data-menuanchor="contact-us"], a[href="#contact-us"]').on('click', function (e) {
              e.preventDefault();
              triggerContact();
            });
            checkLogo(destination);
          }
        });

        if ($('.connect-flower').length) {
          $('.connect-flower').on('mouseenter click', function (e) {
            e.preventDefault();
            triggerContact();
          });
        }

        $('.section').each(function () {
          var $thisSection = this;
          if ($('.fp-next', $thisSection).length) {
            var $nextSlide = $('.slide:first-child', $thisSection).next('.slide');
            var nextTitle = $('.section-title', $nextSlide).html();
            $('.fp-next', $thisSection).html(nextTitle);
          }
        });
        if ($('.next-page-button').length) {
          $('.next-page-button').on('click', function () {
            $('#fullPage').fullpage.moveSectionDown();
          });
        }
      }

      function checkLogo(index) {
        var $headerLogo = $('.header-logo-container .header-text');
        if (index === 1) {
          if (!$($headerLogo).is(':visible')) {
            $($headerLogo).slideDown();
          }
        } else {
          if ($($headerLogo).is(':visible')) {
            $($headerLogo).slideUp();
          }
        }
      }

      if ($('.sub-sub-title').length) {
        $('.sub-sub-title').on('click', function (e) {
          e.preventDefault();
          var $subContent = $(this).next('.sub-sub-content-wrapper');
          if ($($subContent).hasClass('active')) {
            $($subContent).removeClass('active');
          } else {
            $('.sub-sub-content-wrapper.active').removeClass('active');
            $($subContent).addClass('active');
          }
          window.dispatchEvent(new Event('resize'));
        });
      }

      if ($('.sub-sub-menu-item').length) {
        $('.sub-sub-menu-item').on('click', function (e) {
          e.preventDefault();
          var toggleSlug = $(this).data('slug');
          if (!$('.sub-sub-section.' + toggleSlug).hasClass('active')) {
            $('.sub-sub-menu-item.active').removeClass('active');
            $(this).addClass('active');
            $('.sub-sub-section.active').removeClass('active');
            $('.sub-sub-section.' + toggleSlug).addClass('active');
          }
          window.dispatchEvent(new Event('resize'));
        });
      }

      if (!isMobile) {
        $('.menu-toggle').on('mouseenter', function () {
          if (!$('body').hasClass('menu-active')) {
            $('body').addClass('menu-active');
          }
        });
      }
      $('.menu-overlay').on('mouseleave', function () {
        if ($('body').hasClass('menu-active')) {
          $('body').removeClass('menu-active');
        }
      });
      $('.menu-toggle').on('click', function () {
        $('body').toggleClass('menu-active');
      });
      $('.connect-flower-toggle').on('click', function () {
        return false;
      });
      $('#contact-info').on('mouseleave', function () {
        if ($('#contact-info').hasClass('active')) {
          $('#contact-info').removeClass('active');
        }
      });
      $('#meeting-button a').on('click', function (e) {
        e.preventDefault();
        $('#book-meeting-wrapper').show();
      });
      if ($('#close-book-meeting').length) {
        $('#close-book-meeting').on('click', function (e) {
          e.preventDefault();
          $('#book-meeting-wrapper').hide();
        });
      }
    });

    if ($('.join-list').length) {
      $('.join-list').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('signup-active');
      });
    }

    if ($('.jw-submit').length) {
      $('.jw-submit').on('click', function (e) {
        e.preventDefault();
        $('.jw-email').removeClass('error');
        var email = $('.jw-email').val();
        if (email.length) {
          $('#newsletter-container').addClass('loading');
          signup_user(email);
        } else {
          $('.jw-email').addClass('error');
        }
      });
    }

    $('#close-live-chat').on('click', function (e) {
      $('body').removeClass('live-chat-active');
    });

    $('#liveagent-button').on('click', function (e) {
      e.preventDefault();
      $('body:not(.live-chat-active)').addClass('live-chat-active');
    });

    window.lhnJsSdkInit = function () {
      lhnJsSdk.setup = {
        application_id: "c111e851-e2cb-47fc-bede-c18c26c49042",
        application_secret: "dtx+wc6dl/tbg5xk3tnrxnaehhia/c0n6qndkzr+kebp2tkozs"
      };
      lhnJsSdk.openCallback = function () {};
      lhnJsSdk.controls = [{
        type: "hoc",
        id: "09321de4-15bb-49d4-9410-475e2eea00df"
      }];
    };

    (function (d, s) {
      var newjs,
          lhnjs = d.getElementsByTagName(s)[0];
      newjs = d.createElement(s);
      newjs.src = "https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js";
      lhnjs.parentNode.insertBefore(newjs, lhnjs);
    })(document, "script");

    function signup_user(email) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn': 'sign_up_user',
          'email': email
        }
      }).done(function (response) {
        $('#newsletter-container').removeClass('loading');
        response = $.parseJSON(response);
        $('#newsletter-signup-text').html('<div class="response-text">' + response.response + '</div>');
        $('.jw-email').val('');
      });
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    // if (!window._laq) {
    //   window._laq = [];
    // }
    // window._laq.push(function(){
    //   liveagent.showWhenOnline('573f2000000G1J5', document.getElementById('liveagent_button_online_573f2000000G1J5'));
    //   liveagent.showWhenOffline('573f2000000G1J5', document.getElementById('liveagent_button_offline_573f2000000G1J5'));
    // });
  });
})(jQuery);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map