<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package proxyfin
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php $tempdir = get_template_directory_uri(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,700" rel="stylesheet">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126138083-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-126138083-1');
	</script>

	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121636920-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-121636920-1');
	</script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div id="header-content">
			<!-- <div class="mobile-logo">
				<?php
				$homeURL = get_home_url().'/#home';
				if(!is_front_page() && !is_home()) {
					$homeURL = get_home_url();
				} ?>
				<a href="<?php echo $homeURL; ?>">
					<?php display_mobile_logo(); ?>
				</a>
			</div> -->
			<div class="header-logo-container">
				<div class="header-logo">
					<a class="anchor-navigation" href="<?php echo is_home() || is_front_page() ? '#' : home_url(); ?>">
						<?php echo display_proxy_infinity(); ?>
					</a>
				</div>
				<div class="header-text">
					<?php echo display_proxy_text(); ?>
				</div>
			</div>
			<div id="language-toggle">
				<?php qtranxf_generateLanguageSelectCode(['type' => 'both']); ?>
			</div>
		</div>
	</header><!-- #masthead -->
	<div class="menu-toggle">
		<div class="menu-toggle-line"></div>
	</div>
	<div class="menu-overlay">
		<div class="menu-container">
			<nav id="site-navigation" class="main-navigation">
				<ul id="anchor-menu">

					<?php
					global $post;
					$navItems = wp_get_nav_menu_items( 'header-menu' );
					foreach($navItems as $navItem) {
						$pageID = $navItem->object_id;
						$icon = get_field('icon', $pageID);
						$link = get_permalink($pageID);
						$slugsArray = [''];
						$menuArray = [''];
						$args = array(
							'post_parent' => $pageID,
							'post_type'   => 'page',
							'numberposts' => -1,
							'post_status' => 'publish',
							'order' => 'ASC',
							'order_by' => 'menu_order'
						);
						$children = get_children( $args ); ?>
						<li class="menu-icon">
							<a class="anchor-navigation" href="<?php echo $pageID != $navItem->object_id ? $link : $link.'#'; ?>">
								<?php if($icon) { ?>
									<img src="<?php echo $icon['sizes']['medium']; ?>"/>
								<?php } ?>
								<?php echo get_the_title($pageID); ?>
							</a>
							<?php
							if($children) { ?>
								<ul>
									<?php
									foreach($children as $child) {
										if($post->ID == $navItem->object_id) {
											// if you're on the current page, add its children to the anchor lists
											array_push($slugsArray, $child->post_name);
											array_push($menuArray, $child->post_title);
										} ?>
										<li data-menuanchor="<?php echo $child->post_name; ?>">
											<a class="anchor-navigation" href="<?php echo $link.'#'.$child->post_name; ?>">
												<?php echo $child->post_title; ?>
											</a>
										</li>
									<?php
									} ?>
								</ul>
								<?php
								if($post->ID == $navItem->object_id) {
									// if you're on the current page, add the objects to javascript
									$menuObj = (object)['slugs' => $slugsArray, 'menu' => $menuArray];
									wp_localize_script('proxyfin-js', 'menuItems', $menuObj );
								}
							} ?>
						</li>
					<?php
					} ?>
				</ul>
			</nav><!-- #site-navigation -->
		</div>
	</div>

	<div id="content" class="site-content">
