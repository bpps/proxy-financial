<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package proxyfin
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
		<div id="footer-wrapper">
			<div id="contact-info-wrapper">
				<div id="contact-info">
					<div id="connect-icons">
						<div id="live-chat-button">
							<!-- <a href=“#” onclick="lhnJsSdk.openHOC();lhnJsSdk.openCallback();return false;"> -->
							<a id="liveagent-button">
								<div class="live-chat-icon">
									<svg width="34px" height="28px" viewBox="0 0 34 28" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									    <!-- Generator: Sketch 51.2 (57519) - http://www.bohemiancoding.com/sketch -->
									    <desc>Created with Sketch.</desc>
									    <defs>
									        <polygon id="path-1" points="0 0 34 0 34 28 0 28"></polygon>
									    </defs>
									    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <g id="Big-Button" transform="translate(-16.000000, -18.000000)">
									            <g id="Icons-/-Chat" transform="translate(16.000000, 15.000000)">
									                <g id="001-support" transform="translate(0.000000, 3.000000)">
									                    <mask id="mask-2" fill="white">
									                        <use xlink:href="#path-1"></use>
									                    </mask>
									                    <g id="Clip-2"></g>
									                    <path d="M31.4241434,0 L7.57545916,0 C6.14463747,0 5,1.17770907 5,2.5784269 L5,17.4108968 C5,18.8435441 6.17653509,19.9893237 7.57545916,19.9893237 L24.0152515,19.9893237 L27.7357456,23.7450618 C27.8945381,23.9044106 28.0855263,24 28.3079153,24 C28.7531901,24 29.1347691,23.6498703 29.1347691,23.1723212 L29.1347691,19.9893237 L31.4241434,19.9893237 C32.8552632,19.9893237 34,18.8116146 34,17.4108968 L34,2.5784269 C34,1.1458791 32.8234649,0 31.4241434,0 Z M11.2322574,12.7639194 C9.92882744,12.7639194 8.84738898,11.6817997 8.84738898,10.3766713 C8.84738898,9.07144337 9.92882744,7.98942316 11.2322574,7.98942316 C12.5361842,7.98942316 13.6171258,9.07144337 13.6171258,10.3766713 C13.6171258,11.6817997 12.5679825,12.7639194 11.2322574,12.7639194 Z M19.5952954,12.7639194 C18.291766,12.7639194 17.2104269,11.6817997 17.2104269,10.3766713 C17.2104269,9.07144337 18.291766,7.98942316 19.5952954,7.98942316 C20.8991228,7.98942316 21.9801638,9.07144337 21.9801638,10.3766713 C21.9801638,11.6817997 20.8991228,12.7639194 19.5952954,12.7639194 Z M27.9263363,12.7639194 C26.6229064,12.7639194 25.5414679,11.6817997 25.5414679,10.3766713 C25.5414679,9.07144337 26.6229064,7.98942316 27.9263363,7.98942316 C29.2302632,7.98942316 30.3112048,9.07144337 30.3112048,10.3766713 C30.3112048,11.6817997 29.2302632,12.7639194 27.9263363,12.7639194 Z" id="Fill-1" fill="#FFFFFF" mask="url(#mask-2)"></path>
									                    <path d="M4.05766847,17.1186439 L4.05766847,12 L1.59353415,12 C0.722326229,12 0.00088944818,12.7726407 0.00088944818,13.7063488 L0.00088944818,23.5573161 C-0.0288769789,24.5555115 0.6922781,25.3281522 1.59353415,25.3281522 L3.00570228,25.3281522 L3.00570228,27.4526123 C3.00570228,27.7426543 3.24627511,28 3.51661436,28 C3.6670428,28 3.78695361,27.9355127 3.8771919,27.8389326 L6.19099168,25.3603456 L16.4073553,25.3603456 C17.2785632,25.3603456 18,24.5877049 18,23.6539968 L18,21.2073014 L7.87349906,21.2073014 C5.77022398,21.2073014 4.05766847,19.3719779 4.05766847,17.1186439 Z" id="Fill-3" fill="#B5B5B5" mask="url(#mask-2)"></path>
									                    <rect id="Rectangle-3" mask="url(#mask-2)" x="0" y="-3" width="34" height="34"></rect>
									                </g>
									            </g>
									        </g>
									    </g>
									</svg>
									</div>
									<span><?php echo get_field('live_chat_now_translate', 'option'); ?></span>
							</a>
						</div>
						<div class="connect-icons-row">
							<div id="client-login-button" class="connect-button">
								<a target="_blank" href="https://wealth.emaplan.com/ema/SignIn"><?php echo get_field('client_login_translate', 'option'); ?></a>
							</div>
							<div id="meeting-button" class="connect-button">
								<a href="#"><?php echo get_field('book_meeting_translate', 'option'); ?></a>
							</div>
							<div id="proxy-login-button" class="connect-button">
								<a target="_blank" href="https://portal.nesfinancial.com/PortalCommon/NoPermission.aspx"><?php echo get_field('proxy_login_translate', 'option'); ?></a>
							</div>
						</div>
					</div>
					<div class="row-1">
						<!-- <a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a> -->
						<div class="social-icons">
							<div class="social-icon social-icon-2">
								<a target="_blank" href="https://wa.me/14078487996">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 9.5 9.5" style="enable-background:new 0 0 9.5 9.5;" xml:space="preserve">
										<style type="text/css">
											.st0{fill-rule:evenodd;clip-rule:evenodd;fill:#00345A;}
										</style>
										<g id="WA_Logo_3_">
											<g>
												<path class="st0" d="M8.1,1.4C7.2,0.5,6,0,4.8,0C2.2,0,0,2.1,0,4.7c0,0.8,0.2,1.6,0.6,2.4L0,9.5l2.5-0.7C3.2,9.3,4,9.5,4.8,9.5h0
													c0,0,0,0,0,0c2.6,0,4.7-2.1,4.7-4.7C9.5,3.5,9,2.3,8.1,1.4z M4.8,8.7L4.8,8.7c-0.7,0-1.4-0.2-2-0.5L2.6,8L1.1,8.4L1.5,7L1.4,6.8
													C1,6.2,0.8,5.5,0.8,4.7c0-2.2,1.8-3.9,3.9-3.9c1.1,0,2,0.4,2.8,1.2c0.7,0.7,1.2,1.7,1.2,2.8C8.7,6.9,6.9,8.7,4.8,8.7z M6.9,5.7
													C6.8,5.7,6.2,5.4,6.1,5.3C6,5.3,5.9,5.3,5.9,5.4C5.8,5.5,5.5,5.8,5.5,5.9c-0.1,0.1-0.1,0.1-0.3,0c-0.1-0.1-0.5-0.2-1-0.6
													C3.9,5,3.7,4.6,3.6,4.5c-0.1-0.1,0-0.2,0.1-0.2C3.7,4.2,3.8,4.1,3.8,4C3.9,4,3.9,3.9,4,3.8c0-0.1,0-0.1,0-0.2
													c0-0.1-0.3-0.6-0.4-0.9C3.5,2.5,3.4,2.6,3.3,2.6c-0.1,0-0.1,0-0.2,0s-0.2,0-0.3,0.1c-0.1,0.1-0.4,0.4-0.4,1s0.4,1.1,0.5,1.2
													s0.8,1.3,2,1.8c0.3,0.1,0.5,0.2,0.7,0.2C5.8,7,6.1,7,6.3,7C6.5,7,7,6.7,7.1,6.4c0.1-0.3,0.1-0.5,0.1-0.6S7,5.8,6.9,5.7z"/>
											</g>
										</g>
									</svg>
								</a>
							</div>
							<div class="social-icon social-icon-1">
								<a target="_blank" href="weixin://contacts/profile/wechatwithcaulkins">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 9.5 9.5" style="enable-background:new 0 0 9.5 9.5;" xml:space="preserve">
										<style type="text/css">
											.st0{fill:#00345A;}
										</style>
										<g>
											<g>
												<g>
													<path class="st0" d="M4.7,2.9C4.5,2.9,4.3,3,4.3,3.2c0,0.2,0.2,0.3,0.4,0.3C4.9,3.6,5,3.4,5,3.2C5,3,4.9,2.9,4.7,2.9z"/>
												</g>
												<g>
													<path class="st0" d="M2.5,2.9C2.3,2.9,2.1,3,2.1,3.2c0,0.2,0.1,0.3,0.3,0.4c0.2,0,0.4-0.1,0.4-0.3C2.8,3,2.7,2.9,2.5,2.9z"/>
												</g>
											</g>
											<g>
												<g>
													<g>
														<path class="st0" d="M8.1,0H1.4C0.6,0,0,0.6,0,1.4v6.7c0,0.8,0.6,1.4,1.4,1.4h6.7c0.8,0,1.4-0.6,1.4-1.4V1.4
															C9.5,0.6,8.9,0,8.1,0z M2.8,6.2c-0.1,0-0.2,0-0.3,0C2.3,6.4,2,6.6,1.7,6.8c0.1-0.3,0.1-0.5,0.2-0.7c0-0.2,0-0.2-0.1-0.3
															c-0.9-0.7-1.3-1.6-1-2.6C1,2.2,1.6,1.6,2.5,1.4c1.2-0.4,2.6,0,3.3,1c0.3,0.4,0.4,0.7,0.5,1.2c-0.8,0-1.4,0.3-2,0.8
															c-0.5,0.5-0.8,1.2-0.7,2C3.4,6.3,3.1,6.3,2.8,6.2z M8.2,7.5C8,7.7,7.9,7.9,8,8.1c0,0.1,0,0.1,0,0.2C7.8,8.2,7.6,8.1,7.4,8
															C7.1,8,6.9,8.1,6.6,8.2C5.9,8.2,5.2,8,4.7,7.5C3.6,6.5,3.8,5,5,4.2c1.1-0.7,2.7-0.5,3.5,0.5C9.1,5.6,9,6.8,8.2,7.5z"/>
													</g>
												</g>
												<g>
													<path class="st0" d="M5.3,5.1C5.2,5.1,5,5.2,5,5.4c0,0.2,0.1,0.3,0.3,0.3c0.2,0,0.3-0.1,0.3-0.3C5.6,5.2,5.5,5.1,5.3,5.1z"/>
												</g>
												<g>
													<path class="st0" d="M7.1,5.1C7,5.1,6.8,5.2,6.8,5.4c0,0.2,0.1,0.3,0.3,0.3c0.2,0,0.3-0.1,0.3-0.3C7.4,5.2,7.3,5.1,7.1,5.1z"/>
												</g>
											</g>
										</g>
									</svg>
								</a>
							</div>
						</div>
						<a class="connect-flower-toggle" target="_blank" href="http://"><?php echo get_field('connect_translate', 'option'); ?></a>
						<div class="social-icons">
							<div class="social-icon social-icon-1">
								<a target="_blank" href="https://www.linkedin.com/company/proxy-financial-services/">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 9.5 9.5" style="enable-background:new 0 0 9.5 9.5;" xml:space="preserve">
										<style type="text/css">
											.st0{fill:#00345A;}
										</style>
										<g>
											<g>
												<g>
													<path class="st0" d="M8.8,0H0.7C0.3,0,0,0.3,0,0.7v8.1c0,0.4,0.3,0.7,0.7,0.7h8.1c0.4,0,0.7-0.3,0.7-0.7V0.7C9.5,0.3,9.2,0,8.8,0
														z M2.8,8.1H1.4V3.6h1.4V8.1z M2.1,2.9c-0.5,0-0.8-0.4-0.8-0.8c0-0.5,0.4-0.8,0.8-0.8c0.5,0,0.8,0.4,0.8,0.8
														C2.9,2.6,2.6,2.9,2.1,2.9z M8.1,8.1H6.7V5.9c0-0.5,0-1.2-0.7-1.2c-0.7,0-0.8,0.6-0.8,1.2v2.2H3.7V3.6h1.4v0.6h0
														c0.2-0.4,0.6-0.7,1.3-0.7c1.4,0,1.7,0.9,1.7,2.2V8.1z"/>
												</g>
											</g>
										</g>
										</svg>
								</a>
							</div>
							<div class="social-icon social-icon-2">
								<a target="_blank" href="https://www.gotomeet.me/Proxyfinancial">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 80.4 84.3" style="enable-background:new 0 0 80.4 84.3;" xml:space="preserve">
										<style type="text/css">
											.st0{fill:#081C2E;}
										</style>
										<g>
											<path class="st0" d="M55.5,45.9c-0.5,0.2-1.1,0.4-1.6,0.6c-1.9,0.5-4-0.3-5.1-1.9c-1.1-1.5-1.1-3.5,0-5c1.2-1.6,3.1-2.4,5.1-1.9
												c0.6,0.2,1.3,0.5,1.9,0.7c3.2,1.1,6.7-0.1,8.4-3c1.7-2.8,1.1-6.3-1.2-8.5c-2.5-2.3-6.3-2.5-9-0.4c-1.8,1.3-2.6,3.1-2.8,5.3
												c-0.3,2.4-2,4-4.3,4.3c-1.9,0.2-3.9-1-4.7-2.7c-0.9-2.1-0.2-4.2,1.7-5.7c0.4-0.3,0.8-0.6,1.1-0.9c2.9-2.7,2.9-7.1,0.1-9.8
												c-2.9-2.8-7.6-2.6-10.2,0.4c-2.6,3-2.1,7.4,1,9.9c0.6,0.4,1.1,0.9,1.6,1.4c1.3,1.5,1.4,3.7,0.4,5.4c-1.1,1.7-3.1,2.5-5,2
												c-2.6-0.7-3.4-2.6-3.8-5c-0.6-4.1-4.5-6.7-8.5-5.7c-3.9,0.9-6.2,5-5,8.7c1.2,3.9,5.5,5.8,9.4,4.3c0.6-0.2,1.3-0.5,1.9-0.6
												c2.7-0.5,5.1,1.2,5.5,3.9c0.4,2.4-1.4,4.8-3.9,5c-1.2,0.1-2.6-0.3-3.8-0.7c-3.9-1.3-8,0.7-9.2,4.5c-1.2,3.8,1.1,7.8,5.1,8.7
												c4,0.9,7.8-1.6,8.5-5.6c0.1-0.5,0.1-1.1,0.3-1.6c0.5-2.3,2.7-3.9,4.9-3.7c2.3,0.2,4,1.9,4.2,4.2c0.2,2-0.9,3.5-2.5,4.6
												c-2.6,1.9-3.5,4.8-2.6,7.7c0.9,2.8,3.6,4.7,6.7,4.8c3.1,0,5.7-1.8,6.7-4.7c1-2.8,0-5.8-2.4-7.7c-0.5-0.4-1-0.8-1.4-1.2
												c-2.4-2.7-1.2-6.6,2.3-7.5c2.9-0.7,5.4,1.3,5.8,4.6c0.5,4.3,4.3,7,8.4,6.2c4-0.8,6.4-4.8,5.3-8.7C63.7,46.5,59.4,44.4,55.5,45.9z"
												/>
											<path class="st0" d="M77.8,42.1C85.3,27,74.6,10.6,58.9,10C54.4,3.6,48.2,0,40.2,0c-8,0-14.3,3.6-18.8,10.1C5,11-4.9,27.8,2.5,42.2
												c-3.4,7.1-3.5,14.2,0.6,21c4.1,6.9,10.4,10.3,18.3,10.9c9.2,13.5,28.7,13.4,37.7,0c7.8-0.7,14.1-4,18.2-10.9
												C81.4,56.4,81.2,49.2,77.8,42.1z M70.2,60.2c-3.1,4.9-8,6.7-15.9,6c-2.4,6.6-7,10.5-14.1,10.5c-7,0-11.7-3.9-14.1-10.5
												c-7.1,1.2-12.8-0.8-16.5-7c-2.6-4.3-2.9-11.3,2.3-17c-4.7-5.6-5.8-11.6-1.8-18c3.2-5,7.8-6.7,15.9-6.2c2.4-6.5,7-10.5,14.1-10.5
												c7.1,0,11.7,3.8,14.2,10.5c7.6-1.2,13.7,1.1,17,8.2c2.7,5.8,1.3,11.1-2.9,15.9C73.2,47.7,74.3,53.8,70.2,60.2z"/>
										</g>
									</svg>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer-right">

			</div>
		</div>
		<div class="footer-gradient">
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php
$departments = get_field('departments', 'option'); ?>
<div id="book-meeting-wrapper">
	<div id="book-meeting-content">
		<div id="book-meeting-header">
			<div id="book-meeting-header-content">
				<h3><?php echo get_field('book_meeting_translate', 'option'); ?></h3>
				<div id="close-book-meeting">
				</div>
			</div>
		</div>
		<div id="book-meeting-container">
			<div id="book-meeting-content-header">
				<?php echo get_field('select_meeting_department', 'options'); ?>
			</div>
			<div id="book-meeting-links">
				<?php
				foreach($departments as $dept) { ?>
					<a class="book-meeting-link" target="_blank" href="<?php echo $dept['book_meeting_link']; ?>"><?php echo $dept['title']; ?></a>
				<?php
				} ?>
			</div>
		</div>
	</div>
</div>
<div id="live-chat-wrapper">
	<div id="live-chat-content">
		<div id="live-chat-header">
			<div id="live-chat-header-content">
				<h3><?php echo get_field('live_chat_translate', 'option'); ?></h3>
				<div id="close-live-chat">
				</div>
			</div>
		</div>
		<div id="live-chat-container">
			<div id="live-chat-loader">
				<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
			<iframe name="livechat" id="livechat">
			</iframe>
			<div id="live-chat-options-container">
				<div id="live-chat-options-header">
					<?php echo get_field('select_live_chat_department', 'options'); ?>
				</div>
				<div id="live-chat-options">
					<?php
					foreach($departments as $dept) { ?>
						<div class="live-chat-option">
							<a class="live-chat-online" id="liveagent_button_online_<?php echo $dept['live_chat_id']; ?>" href="javascript://Chat" style="display: none;" onclick="liveagent.startChatWithWindow('<?php echo $dept['live_chat_id']; ?>', 'livechat');showChatWindow();">
								<?php echo $dept['title']; ?> <span>ONLINE</span>
							</a>
							<div class="live-chat-offline" id="liveagent_button_offline_<?php echo $dept['live_chat_id']; ?>" style="display: none;">
								<?php echo $dept['title']; ?> <span>OFFLINE</span>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type='text/javascript' src='https://c.la4-c2-phx.salesforceliveagent.com/content/g/js/43.0/deployment.js'></script>
<script type='text/javascript'>
liveagent.init('https://d.la4-c2-phx.salesforceliveagent.com/chat', '572f2000000Fzza', '00Df2000001A9bh');
</script>
<script type="text/javascript">
	if (!window._laq) {
		window._laq = [];
	}
	window._laq.push(function(){
		<?php
		foreach($departments as $dept) { ?>
			liveagent.showWhenOnline('<?php echo $dept['live_chat_id']; ?>', document.getElementById('liveagent_button_online_<?php echo $dept['live_chat_id']; ?>'));
			liveagent.showWhenOffline('<?php echo $dept['live_chat_id']; ?>', document.getElementById('liveagent_button_offline_<?php echo $dept['live_chat_id']; ?>'));
		<?php
		} ?>
	});

	function showChatWindow() {
		jQuery('#live-chat-options-container').fadeOut();
	}
</script>

<div id="fixed-background">
	<!-- <div class="yellow-bg">
	</div> -->
	<div class="circle-overlay">
	</div>
</div>

<?php
$ref = preg_replace('#^https?://#', '', trim(wp_get_referer()));
$homeurl = preg_replace('#^https?://#', '', trim(home_url('/')));
if ($ref !== '' && strpos(parse_url($ref), parse_url($homeurl)) !== false) {
  // Internal URL ?>
	<style>
		body.loaded .site-footer {
			transition-delay: .5s!important;
		}
	</style>
	<?php
}else{
	// External URL ?>
	<div class="intro-overlay">
		<div class="intro-logo-container">
			<div class="intro-logos">
				<!-- <div class="intro-logo-background">
					<div class="logo-background-infinity">
							<?php echo display_proxy_infinity(); ?>
					</div>
					<div class="logo-background-text">
						<?php echo display_proxy_text(); ?>
					</div>
				</div> -->
				<div class="intro-logo">
					<?php display_proxy_logo(); ?>
				</div>
			</div>
		</div>
	</div>
<?php
} ?>


<div id="circles-backgroud">
	<div id="circles-content">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 510.7 508.7" style="enable-background:new 0 0 510.7 508.7;" xml:space="preserve">
			<circle class="st0" style="fill:#C0C5CE;" cx="759.9" cy="205.8" r="1.4"/>
			<g>
				<polygon class="circle-square st1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" points="255.6,128.1 131.2,254.6 255.6,378.5 381.5,254.6 	"/>
				<circle class="st1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" cx="130.1" cy="254.3" r="125.2"/>
				<circle class="st1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" cx="380.5" cy="254.3" r="125.2"/>
				<circle class="st1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" cx="255.3" cy="379.5" r="125.2"/>
				<circle class="st1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" cx="255.3" cy="129.1" r="125.2"/>
				<line class="st1 circle-line-1" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" x1="255.6" y1="128.1" x2="255.6" y2="378.5"/>
				<line class="st1 circle-line-2" style="fill:none;stroke:#C0C5CE;stroke-miterlimit:10;" x1="131.2" y1="254.6" x2="381.5" y2="254.6"/>
				<g>
					<circle class="st0" style="fill:#C0C5CE;" cx="131.2" cy="254.6" r="5.7"/>
					<circle class="st0" style="fill:#C0C5CE;" cx="381.5" cy="254.6" r="5.7"/>
					<circle class="st0" style="fill:#C0C5CE;" cx="255.6" cy="378.5" r="5.7"/>
					<circle class="st0" style="fill:#C0C5CE;" cx="255.6" cy="128.1" r="5.7"/>
				</g>
			</g>
			</svg>
	</div>
</div>

<!-- <section id="meeting-container">
	<div id="meeting-wrapper">
		<div id="meeting-content">
			<a href="http://" id="close-meeting">
			</a>
			<iframe src="https://proxy-cj.youcanbook.me/?noframe=true&skipHeaderFooter=true" id="ycbmiframeproxy-cj" frameborder="0" allowtransparency="true"></iframe>
			<script>
				window.addEventListener && window.addEventListener("message", function(event){
					if (event.origin === "https://proxy-cj.youcanbook.me"){
						document.getElementById("ycbmiframeproxy-cj").style.height = event.data + "px";
					}
				}, false);
			</script>
		</div>
	</div>
</section> -->

<?php wp_footer(); ?>

</body>
</html>
