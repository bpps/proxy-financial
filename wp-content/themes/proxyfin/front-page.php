<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package proxyfin
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post(); ?>
				<div id="fullpage" class="hidden">
					<div class="loader">
						<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
					</div>
					<section data-pageid="<?php the_ID(); ?>" class="section first-section fp-auto-height" id="section-proxy-financial">
						<div class="section-wrapper">
							<?php the_content(); ?>
						</div>
					</section>
					<?php echo get_page_sections(get_the_ID()); ?>
				</div>
				<?php
				// $sections = get_field('sections', get_option('page_on_front'));
				// echo get_page_sections($sections);
			endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
